import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.igext as ig
import geni.rspec.emulab.pnext as PN
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.emulab.route as route

tour_description = """
Deploy a set of dense site radios, fixed endpoint radios, and rooftop X310 SDRs with compute nodes with GnuRadio installed. Optionally, include mobile endpoints in the experiment. (Only enable if this experiment is meant to last until the end of the day. Otherwise, instantiate a separate experiment using the mobile endpoints profile.) You can also specify frequency ranges to transmit in, if that is desired, and you've already made a corresponding spectrum reservation.
"""

tour_instructions = """
"""

UBUNTU_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
COTS_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-jammy-image"
COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"

def dense_site(dense_site_id):
    node = request.RawPC("{}".format(dense_site_id))
    node.component_manager_id = COMP_MANAGER_ID
    node.component_id = dense_site_id
    node.disk_image = UBUNTU_IMG
    node.addService(pg.Execute(shell="bash", command="/local/repository/bin/deploy-gnuradio.sh"))
    node.startVNC()

def fixed_endpoint(fe_node_tuple):
    fe_id = fe_node_tuple.split(":")[0]
    component_id = fe_node_tuple.split(":")[1]
    node = request.RawPC("{}-{}".format(fe_id, component_id))
    node.component_manager_id = "urn:publicid:IDN+{}.powderwireless.net+authority+cm".format(fe_id)
    node.component_id = component_id
    node.disk_image = UBUNTU_IMG if component_id == "nuc2" else COTS_UE_IMG
    node.addService(pg.Execute(shell="bash", command="/local/repository/bin/deploy-gnuradio.sh"))
    node.startVNC()

def x310_node_pair(x310_radio):
    node = request.RawPC("{}-x310-comp".format(x310_radio.split("-")[1]))
    node.component_manager_id = COMP_MANAGER_ID
    node.hardware_type = params.x310_compute_node_type
    node.disk_image = UBUNTU_IMG
    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(pg.IPv4Address("192.168.40.1", "255.255.255.0"))
    node.startVNC()

    radio = request.RawPC("{}".format(x310_radio))
    radio.component_id = x310_radio
    radio.component_manager_id = COMP_MANAGER_ID

    radio_link = request.Link("{}-link".format(x310_radio))
    radio_link.bandwidth = 10*1000*1000
    radio_link.addInterface(node_radio_if)
    radio_link.addNode(radio)

    node.addService(pg.Execute(shell="bash", command="/local/repository/bin/deploy-gnuradio.sh"))
    node.addService(pg.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))

pc = portal.Context()

node_types = [
    ("d430", "Emulab, d430"),
    ("d740", "Emulab, d740"),
]
pc.defineParameter(
    name="x310_compute_node_type",
    description="Type of compute node to pair with X310 SDRs",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types
)

pc.defineParameter(
    name="include_mobiles",
    description="Include mobile endpoints in the experiment. (Only enable if this experiment is" \
                "meant to last until the end of the day. Otherwise, instantiate a separate" \
                "experiment using the mobile endpoints profile.)",
    typ=portal.ParameterType.BOOLEAN,
    defaultValue=False
)

dense_radios = [
    ("cnode-mario", "Mario"),
    ("cnode-moran", "Moran"),
    ("cnode-guesthouse", "Guesthouse"),
    ("cnode-ebc", "EBC"),
    ("cnode-ustar", "USTAR"),
]
pc.defineStructParameter(
    "dense_radios", "Dense Site Radios", [],
    multiValue=True,
    min=0,
    multiValueTitle="Dense Site NUC+B210 radios to allocate.",
    members=[
        portal.Parameter(
            "device",
            "SFF Compute + NI B210 device",
            portal.ParameterType.STRING,
            dense_radios[0], dense_radios,
            longDescription="A Small Form Factor compute with attached NI B210 device at the given" \
                            "Dense Deployment site will be allocated."
        ),
    ]
)

fixed_endpoint_nodes = [
    ("bookstore:nuc1", "Bookstore, nuc1"),
    ("bookstore:nuc2", "Bookstore, nuc2"),
    ("cpg:nuc1", "Central Parking Garage, nuc1"),
    ("cpg:nuc2", "Central Parking Garage, nuc2"),
    ("ebc:nuc1", "EBC, nuc1"),
    ("ebc:nuc2", "EBC, nuc2"),
    ("guesthouse:nuc1", "Guest House, nuc1"),
    ("guesthouse:nuc2", "Guest House, nuc2"),
    ("humanities:nuc1", "Humanities, nuc1"),
    ("humanities:nuc2", "Humanities, nuc2"),
    ("law73:nuc1", "Law 73, nuc1"),
    ("law73:nuc2", "Law 73, nuc2"),
    ("madsen:nuc1", "Madsen, nuc1"),
    ("madsen:nuc2", "Madsen, nuc2"),
    ("moran:nuc1", "Moran, nuc1"),
    ("moran:nuc2", "Moran, nuc2"),
    ("sagepoint:nuc1", "Sage Point, nuc1"),
    ("sagepoint:nuc2", "Sage Point, nuc2"),
    ("web:nuc1", "WEB, nuc1"),
    ("web:nuc2", "WEB, nuc2"),
]
pc.defineStructParameter(
    "fixed_endpoint_nodes", "Fixed Endpoint Nodes", [],
    multiValue=True,
    min=0,
    multiValueTitle="Fixed endpoint NUC+B210/COTSUE radios to allocate.",
    members=[
        portal.Parameter(
            "device",
            "SFF Compute + NI B210 device (and COTS UE if nuc1)",
            portal.ParameterType.STRING,
            fixed_endpoint_nodes[0], fixed_endpoint_nodes,
            longDescription="A small form factor compute with attached NI B210 (and COTS UE if nuc1)" \
                            "at the selected Fixed Endpoint site will be allocated."
        ),
    ]
)

rooftop_sdrs = [
    ("cbrssdr1-browning", "Browning, CBRS antenna"),
    ("cbrssdr1-bes", "Behavioral, CBRS antenna"),
    ("cbrssdr1-dentistry", "Dentistry, CBRS antenna"),
    ("cbrssdr1-fm", "Friendship Manor, CBRS antenna"),
    ("cbrssdr1-honors", "Honors, CBRS antenna"),
    ("cbrssdr1-meb", "MEB, CBRS antenna"),
    ("cbrssdr1-smt", "SMT, CBRS antenna"),
    ("cbrssdr1-hospital", "Hospital, CBRS antenna"),
    ("cbrssdr1-ustar", "USTAR, CBRS antenna"),
    ("cellsdr1-browning", "Browning, wideband antenna"),
    ("cellsdr1-bes", "Behavioral, wideband antenna"),
    # ("cellsdr1-dentistry", "Dentistry, wideband antenna"),
    # ("cellsdr1-fm", "Friendship Manor, wideband antenna"),
    # ("cellsdr1-honors", "Honors, wideband antenna"),
    ("cellsdr1-meb", "MEB, wideband antenna"),
    # ("cellsdr1-smt", "SMT, wideband antenna"),
    ("cellsdr1-hospital", "Hospital, wideband antenna"),
    # ("cellsdr1-ustar", "USTAR, wideband antenna"),
]
pc.defineStructParameter(
    "rooftop_x310s", "Rooftop X310s", [],
    multiValue=True,
    min=0,
    multiValueTitle="Rooftop X310s to allocate.",
    members=[
        portal.Parameter(
            "device",
            "Rooftop X310",
            portal.ParameterType.STRING,
            rooftop_sdrs[0], rooftop_sdrs,
            longDescription="The selected rooftop X310 will be allocated and paired with a compute node" \
                            "with GnuRadio installed."
        ),
    ]
)

pc.defineStructParameter(
    "freq_ranges", "Frequency Ranges To Transmit In",
    defaultValue=[],
    multiValue=True,
    min=0,
    multiValueTitle="Frequency ranges to be used for transmission (if you plan to transmit).",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Range Min",
            portal.ParameterType.BANDWIDTH,
            3440.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Range Max",
            portal.ParameterType.BANDWIDTH,
            3450.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ]
)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

for dense_radio in params.dense_radios:
    dense_site(dense_radio.device)

for fe_node in params.fixed_endpoint_nodes:
    fixed_endpoint(fe_node.device)

for rooftop_x310 in params.rooftop_x310s:
    x310_node_pair(rooftop_x310.device)

for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 0)

if params.include_mobiles:
    all_routes = request.requestAllRoutes()
    all_routes.disk_image = COTS_UE_IMG
    all_routes.startVNC()

tour = ig.Tour()
tour.Description(ig.Tour.MARKDOWN, tour_description)
tour.Instructions(ig.Tour.MARKDOWN, tour_instructions)
request.addTour(tour)

pc.printRequestRSpec(request)
